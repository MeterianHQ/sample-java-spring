### Meterian Java Spring sample project
This is a sample Java project, based on Spring, that shows the Meterian integration. You can see the produced report [here](https://www.meterian.com/projects/?pid=9c7ac483-8961-404e-9950-ce87ffafba2f). 

This project uses the [dockerized client](https://docs.meterian.io/the-meterian-client-dockerized), which contains all the required tools, to show a working implementation of the scanner in a pipeline.

Documentation about the GitLab integration can be found [here](https://docs.meterian.io/ci-server-integrations/gitlabcicd)


### Where this code come from?
This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html), we are not responsibile for the vulnerabile components it contains.



